/**
 * Created by marco on 02/10/15.
 */
$(document).ready(function() {

    $("#about-btn").click( function(event) {
        alert("You clicked the button using JQuery!");
    });

    $("p").hover(
        function() {
            $(this).css('color', 'red');
        },
        function() {
            $(this).css('color', 'blue');
        });
    //se utilizar "p" no lugar de "this" o que ocorre?
    //    - Ele muda pra todos os "p" e no apenas para o corrente.

    $("#about-btn").addClass('btn btn-primary')

    $("#about-btn").click( function(event) {
        msgstr = $("#msg").html()
        msgstr = msgstr + "!"
        $("#msg").html(msgstr)
    });
});