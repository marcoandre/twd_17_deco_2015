#!/usr/bin/env python
#coding:utf-8

import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tango_with_django_project.settings')

import django
django.setup()

from rango.models import Category, Page

def populate():
    category = add_category(name='Python', views=128, likes=64)

    add_page(cat=category,
        title="Official Python Tutorial",
        url="http://docs.python.org/2/tutorial/",
        views=10)

    add_page(cat=category,
        title="How to Think like a Computer Scientist",
        url="http://www.greenteapress.com/thinkpython/",
        views=8)

    add_page(cat=category,
        title="Learn Python in 10 Minutes",
        url="http://www.korokithakis.net/tutorials/python/",
        views=2)

    category = add_category("Django", 64, 32)

    add_page(cat=category,
        title="Official Django Tutorial",
        url="https://docs.djangoproject.com/en/1.5/intro/tutorial01/",
        views=9)

    add_page(cat=category,
        title="Django Rocks",
        url="http://www.djangorocks.com/")

    add_page(cat=category,
        title="How to Tango with Django",
        url="http://www.tangowithdjango.com/",
        views=8)

    category = add_category(likes=16, name="Other Frameworks", views=32)

    add_page(cat=category,
        title="Bottle",
        url="http://bottlepy.org/docs/dev/")

    add_page(cat=category,
        title="Flask",
        url="http://flask.pocoo.org")

    # Print out what we have added to the user.
    for c in Category.objects.all():
        for p in Page.objects.filter(category=c):
            print "- {0} - {1}".format(str(c), str(p))

def add_page(cat, title, url, views=0):
    p = Page.objects.get_or_create(category=cat, title=title)[0]
    p.url=url
    p.views=views
    p.save()
    return p

def add_category(name, views, likes):
    c = Category.objects.get_or_create(name=name)[0]
    c.views = views
    c.likes = likes
    c.save()
    return c

# Start execution here!
if __name__ == '__main__':
    print "Starting Rango population script..."
    populate()
