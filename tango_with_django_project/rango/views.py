#coding: utf-8
import datetime

from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from models import Category, Page, User, UserProfile
from forms import CategoryForm, PageForm, UserForm, UserProfileForm

def index(request):
    #print(request)
    # request.session.set_test_cookie()
    context_dict = {}

    category_list = Category.objects.order_by('-likes')[:5]
    context_dict['categories'] = category_list

    page_list = Page.objects.order_by('-views')[:5]
    context_dict['pages'] = page_list

    # print context_dict

    #Contador de visitas
    visits = request.session.get('visits')
    if not visits:
        visits = 1
    reset_last_visit_time = False

    last_visit = request.session.get('last_visit')
    if last_visit:
        last_visit_time = datetime.datetime.strptime(last_visit[:-7], "%Y-%m-%d %H:%M:%S")

        if (datetime.datetime.now() - last_visit_time).seconds > 5:
            visits += 1
            reset_last_visit_time = True
    else:
        reset_last_visit_time = True

    if reset_last_visit_time:
        request.session['last_visit'] = str(datetime.datetime.now())
        request.session['visits'] = visits

    context_dict['visits'] = visits
    # print(visits)

    return render(request, 'rango/index.html', context_dict)


def about(request):
    if request.session.get('visits'):
        count = request.session.get('visits')
    else:
        count = 0

    # remember to include the visit data
    return render(request, 'rango/about.html', {'visits': count})

def category(request, category_name_slug):
    #print(category_name_slug)
    context_dict = {}

    try:
        category = Category.objects.get(slug=category_name_slug)
        context_dict['category_name'] = category.name
        pages = Page.objects.filter(category=category).order_by('-views')
        context_dict['pages'] = pages
        context_dict['category'] = category
    except Category.DoesNotExist:
        pass

    #print(context_dict)
    return render(request, 'rango/category.html', context_dict)

@login_required
def add_category(request):
    if request.method != 'POST':
        form = CategoryForm()
    else:
        form = CategoryForm(request.POST)

        if form.is_valid():
            cat = form.save(commit=True)
            print(cat, cat.slug)
            return index(request)
        else:
            print form.errors

    return render(request, 'rango/add_category.html', {'form': form})

@login_required
def add_page(request, category_name_slug):
    try:
        cat = Category.objects.get(slug=category_name_slug)
    except Category.DoesNotExist:
        cat = None

    if request.method != 'POST':
        form = PageForm()
    else:
        form = PageForm(request.POST)
        if form.is_valid():
            if cat:
                page = form.save(commit=False)
                page.category = cat
                page.views = 0
                page.save()
                return category(request, category_name_slug)
            else:
                print form.errors

    context_dict = {'form':form, 'category':cat}

    return render(request, 'rango/add_page.html', context_dict)

def register(request):
    # if request.session.test_cookie_worked():
    #     print(">>>>> Teste de cookie funcionou!!!")
    #     request.session.delete_test_cookie()

    context_dict = {}

    registered = False

    if request.method != "POST":
        user_form = UserForm()
        profile_form = UserProfileForm()
    else:
        user_form = UserForm(request.POST)
        profile_form = UserProfileForm(request.POST)

        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()

            profile = profile_form.save(commit=False)
            profile.user = user

            if 'picture' in request.FILES:
                profile.picture = request.FILES['picture']

            profile.save()

            registered = True
        else:
            print user_form.errors, profile_form.errors

    context_dict['user_form'] = user_form
    context_dict['profile_form'] = profile_form
    context_dict['registered'] = registered

    return render(request, 'rango/register.html', context_dict)

def profile_edit(request, username):
    context_dict = {}

    if request.method != "POST":
        u = User.objects.get(username=username)
        user_form = UserForm(instance=u)
        up = UserProfile.objects.get(user=u)
        if up:
            profile_form = UserProfileForm(instance=up)
        print(username, u.username, up.website)
    else:
        user_form = UserForm(request.POST)
        profile_form = UserProfileForm(request.POST)

        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()

            profile = profile_form.save(commit=False)
            profile.user = user

            if 'picture' in request.FILES:
                profile.picture = request.FILES['picture']

            profile.save()

    context_dict['user_form'] = user_form
    context_dict['profile_form'] = profile_form

    return render(request, 'rango/user_edit.html', context_dict)


def user_login(request):
    context_dict = {}

    if request.method != "POST":
        return render(request, 'rango/login.html', context_dict)
    else:
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/rango/')
            else:
                return HttpResponse('Sua conta no Rango está desabilitada.')
        else:
            erro = 'Detalhes de login inválidos foram fornecidos: usuário: {0}, senha: {1}'.format(username, password)
            print(erro)
            return HttpResponse(erro)

@login_required
def restricted(request):
    return render(request, 'rango/restricted.html', {})

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/rango/')

def track_url(request):
    if request.method == 'GET':
        if 'page_id' in request.GET:
            page_id = request.GET['page_id']
            try:
                page = Page.objects.get(id=page_id)
                page.views += 1
                page.save()
                return redirect(page.url)
            except:
                pass
    return redirect('/rango/')

@login_required
def profile(request, username):
    context = {}

    u = User.objects.get(username=username)

    try:
        up = UserProfile.objects.get(user=u)
    except:
        up = None

    context['u'] = u
    context['userprofile'] = up

    return render(request, 'rango/profile.html', context)

@login_required
def users_list(request):
    context = {}
    users = User.objects.order_by('username')


    context['users'] = users
    return render(request, 'rango/users_list.html', context)

@login_required
def like_category(request):

    cat_id = None
    if request.method == 'GET':
        cat_id = request.GET['category_id']

    likes = 0
    if cat_id:
        cat = Category.objects.get(id=int(cat_id))
        if cat:
            likes = cat.likes + 1
            cat.likes =  likes
            cat.save()

    return HttpResponse(likes)

def get_category_list(max_results=0, starts_with=''):
        cat_list = []
        if starts_with:
                cat_list = Category.objects.filter(name__istartswith=starts_with)

        if max_results > 0:
                if len(cat_list) > max_results:
                        cat_list = cat_list[:max_results]

        return cat_list

def suggest_category(request):
        cats = []
        starts_with = ''
        if request.method == 'GET':
                starts_with = request.GET['suggestion']
        cats = get_category_list(8, starts_with)

        context_dict = {'cats': cats }
        return render(request, 'rango/cats.html', context_dict)