from django.contrib import admin
from models import Category, Page,UserProfile

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'views', 'likes')
    ordering = ('-views',)

    prepopulated_fields = {'slug':('name',)}

class PageAdmin(admin.ModelAdmin):
    list_display = ('title', 'category', 'views')
    ordering = ('-views',)

admin.site.register(Category,CategoryAdmin)
admin.site.register(Page, PageAdmin)
admin.site.register(UserProfile)